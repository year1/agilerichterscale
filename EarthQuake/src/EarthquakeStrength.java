import java.util.Scanner;

public class EarthquakeStrength 
{
	public static void main(String[] args) 
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter the Richter Scale : ");
		double input = sc.nextDouble();
		System.out.println(scale(input));
		sc.close();
	}
	public static String scale(double scale)
	{
		if (scale < 0 || scale >= 10.0)
		{
			return "ERROR! Value out of range.";
		}
		if (scale < 4.5)
		{
			return "No destruction of buildings";
		}
		else if (scale < 6.0)
		{
			return "Damage to poorly constructed Building";
		}
		else if (scale < 7.0)
		{
			return "Many buildings considerably damaged, some collapse";
		}
		else if (scale < 8.0)
		{
			return "Many buildings destroyed";
		}
		else
		{
			return "Most structures fall";
		}
	}
}
